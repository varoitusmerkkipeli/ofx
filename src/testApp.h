#pragma once

#include "ofMain.h"
//#include "ofxMidi.h"
#include "ofxSvg.h"
#include "ofxFidMain.h"
#include "ofxCvHaarFinder.h"
#include "ofxKinect.h"

#include <fstream>

#define MAX_POINTS 27

class testApp : public ofBaseApp {

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

//        void newMidiMessage(ofxMidiMessage& msg);
        void initNewTask(int t);
        //void correctAnswer();
        //void incorrectAnswer();
        void lockAnswer(bool correct);
        void announceWIN();
        void announceFAIL();
        void loadTextFromFile(string filename);
        void updateStaticContent();
        void updateFbo();
        ofFbo staticContent;
        bool fboMode;
        bool fboUpdateRequested;

        int itemsOnTable[12];

        void newGame();

//		ofxMidiIn * midiIn;
//		ofxMidiOut * midiOut;

        vector <ofImage> images;

        ofImage background;
        ofImage staticBG;

        bool signTableVisible;
        ofImage signTableBackground;
        vector <ofxSVG> signs;
        bool visibleSigns[9];
        int signActive;

        bool scoreBoardVisible;
        vector <ofImage> scoreBoard;
        int points;

        bool speechBubbleVisible;
        vector <ofImage> speechBubble;
        #define SPEECH_BUBBLE_CORRECT_ANSWER 0
        #define SPEECH_BUBBLE_WRONG_ANSWER 1
        #define SPEECH_BUBBLE_TASK 2
        #define SPEECH_BUBBLE_TASK_1_ARROW_CORRECT 3
        #define SPEECH_BUBBLE_TASK_1_ARROW_WRONG 4
        int bubble;

		int state;
		short task;
		bool taskCompleted[11];
		short taskState;
		#define TASK_STATE_TASK_GIVEN 0
		#define TASK_STATE_WRONG_ANSWER_GIVEN 1
		#define TASK_STATE_CORRECT_ANSWER_GIVEN 2
		#define TASK_STATE_CUT_SCENE 3
		#define TASK_STATE_PLEASE_CLEAR_THE_TABLE 4
		#define TASK_FINISHED_TRANSITION 5
		#define TASK_STATE_JUDGEMENT_DELAY 6
		long advanceTimer;
		long lastActionOnTable;
		#define DELAY_WRONG_ANSWER 8000
		#define DELAY_CORRECT_ANSWER 15000
		#define DELAY_CUT_SCENE 15000
		#define DELAY_PER_LETTER 20
		bool answerWasCorrect;
		int startHysteresis;

        vector <ofImage> graphics;
        #define TASK_2_NOTHING 0
        #define TASK_2_GLOVES 1
        #define TASK_2_GOGGLES 2
        #define TASK_2_GLOVES_GOGGLES 3
        #define TASK_2_EARMUFFS 4
        #define TASK_2_EARMUFFS_GLOVES 5
        #define TASK_2_EARMUFFS_GOGGLES 6
        #define TASK_3_ACETONE 7
        #define GRAPHICS_HNO3 8
        #define GRAPHICS_HNO3_OK 9
        #define GRAPHICS_HNO3_ARROWS 10
        #define GRAPHICS_QUESTION 11
        #define GRAPHICS_RED_NOT 12
        #define GRAPHICS_PAINT 13
        #define GRAPHICS_PAINT_ARROWS 14
        #define GRAPHICS_TOILET_TERROR 15
        #define GRAPHICS_DOCK_TERROR 16
        #define GRAPHICS_PAINT_2_TOILET 17
        #define GRAPHICS_PAINT_2_WATER 18
        #define GRAPHICS_PAINT_2_CONTAINER 19
        #define GRAPHICS_RED_NOT_TINY 20
        #define GRAPHICS_BLANK_LABEL 21
        #define GRAPHICS_STAR 22
        #define GRAPHICS_H2O 23
        #define GRAPHICS_H2O_ARROWS 24
        #define GRAPHICS_H2O_QUESTION 25
        #define GRAPHICS_REMOVE_OBJECTS 26

        bool task_3_acetone_on_table = false;
        bool explotion = false;

        #define ITEM_SHOUT 0
        #define ITEM_GLASSES 1
        #define ITEM_GLOVES 2
        #define ITEM_EARMUFFS 3
        #define ITEM_ACETONE 4
        #define ITEM_MATCHES 5
        #define ITEM_FLASHLIGHT 6
        #define ITEM_SODASTREAM 7
        #define ITEM_HNO3 8
        #define ITEM_PAINT 9
        #define ITEM_STAR 10
        #define ITEM_POISON 11
        int lastItemOnTable;

        bool slotsGuessed[10];
        #define SLOT_TASK_6_1 0
        #define SLOT_TASK_6_2 1
        #define SLOT_TASK_7_1 2
        #define SLOT_TASK_7_2 3
        #define SLOT_TASK_8_1 4
        #define SLOT_TASK_8_2 5
        #define SLOT_TASK_8_3 6
        #define SLOT_TASK_2_GOGGLES 7
        #define SLOT_TASK_2_GLOVES 8
        int lastSlotGuessed;

        int postMouseX, postMouseY;
        long mouseMovedLast;
        float cursorX, cursorY;
        bool cursorVisible;
        int paintGoingTo;
        #define PAINT_2_TOILET 0
        #define PAINT_2_WATER 1
        #define PAINT_2_CONTAINER 2
        bool draggin;
        bool rescan;

        ofTrueTypeFont font;
        string textString;


		ofxCvGrayscaleImage grayImage;
		ofxCvGrayscaleImage grayBg;
		ofxCvGrayscaleImage	grayDiff;
		ofxCvColorImage		colorImg;
		bool drawGrayDiff;
		ofxCvGrayscaleImage depthImage;
		bool drawDepthImage;
		ofxCvGrayscaleImage depthStart;
		int shouldIStart;
		bool depthFilterOn;
		bool debugGraphics;
		//ofxCvContourFinder contourFinder;

		#define ROI_X 181
		#define ROI_Y 0
		#define ROI_W 300
		#define ROI_H 350

		#define DEPTH_ROI_X 285
		#define DEPTH_ROI_Y 411
		#define DEPTH_ROI_W 20
		#define DEPTH_ROI_H 20

		ofxKinect kinect;

		int nearT, farT;

		ofxFiducialTracker	fidfinder;
		float postFidX, postFidY;

		int 				threshold;
		bool				bLearnBakground;
		bool				backgroundSubOn;

		vector <ofSoundPlayer> sounds;
		#define MAX_SOUNDS 20
		#define SOUNDS_CORRECT 0
		#define SOUNDS_FAIL 1
		#define SOUNDS_CUTSCENE_2 2
		#define SOUNDS_CUTSCENE_3 3
		#define SOUNDS_CUTSCENE_4 4
		#define SOUNDS_CUTSCENE_5 5
		#define SOUNDS_TASK_3_FIRE 6
		#define SOUNDS_TASK_2_ALARM 7
		#define SOUNDS_TASK_4_EXPLOSION 8
		#define SOUNDS_TASK_4_FACTORY 9
		#define SOUNDS_TASK_5_BOTTLE 10
		#define SOUNDS_TASK_7_NATURA 11
		#define SOUNDS_TASK_9_ATTIC 12
		#define SOUNDS_SNAP 13
		#define SOUNDS_CUTSCENE_1 14

		void silence();

        bool advanceRequested;

    private:
        bool anyItemOnTable();
        int howManyItemsOnTable();
        bool projection(float x1, float y1, float x2, float y2, float a1, float b1, float * a2, float * b2, bool below, bool s, bool e);

};
