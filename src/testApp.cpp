#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup()
{
    ofSetFrameRate(30);

    kinect.setRegistration(true);
    kinect.setDepthClipping(500,3000);
    kinect.init();
    kinect.open();

    if(kinect.isConnected())
    {
        ofLogNotice() << "KINECT connected!";
    }
    else
    {
        //exit();
        ofLogWarning() << "KINECT NOT connected!";
    }

    ofHideCursor();

    colorImg.allocate(640,480);
    colorImg.setROI(ROI_X,ROI_Y,ROI_W,ROI_H);
    grayImage.allocate(ROI_W,ROI_H);
    grayImage.set(0);

    depthImage.allocate(640,480);
    depthImage.setROI(DEPTH_ROI_X,DEPTH_ROI_Y,DEPTH_ROI_W,DEPTH_ROI_H);
    depthStart.allocate(DEPTH_ROI_W,DEPTH_ROI_H);

    staticContent.allocate(1280,800,GL_RGBA);

    threshold = 175;

    fidfinder.initTree(ofToDataPath("minix2.trees").c_str());
    fidfinder.findFiducials( grayImage );

    ofLogNotice() << "Loading SVG graphics.";
    signTableBackground.loadImage("./images/img_MERKIT/merkit_bg_1280x140.png");
    signTableVisible = true;
    for (int i = 0; i < 12; i++)
    {
        signs.push_back(ofxSVG());
    }
    signs[0].load("./images/img_MERKIT/merkit_teht1_merkkikuva_terveyshaitta.svg");
    signs[1].load("./images/img_MERKIT/merkit_teht2_merkkikuva_syovyttava.svg");
    signs[2].load("./images/img_MERKIT/merkit_teht3_merkkikuva_syttyva.svg");
    signs[3].load("./images/img_MERKIT/merkit_teht4_merkkikuva_rajahde.svg");
    signs[4].load("./images/img_MERKIT/merkit_teht5_merkkikuva_painekaasut.svg");
    signs[5].load("./images/img_MERKIT/merkit_teht6_merkkikuva_hapettava.svg");
    signs[6].load("./images/img_MERKIT/merkit_teht7_merkkikuva_ymparistovaara.svg");
    signs[7].load("./images/img_MERKIT/merkit_teht8_merkkikuva_krooninentervhaitta.svg");
    signs[8].load("./images/img_MERKIT/merkit_teht9_merkkikuva_myrkyllisyys.svg");
    signs[9].load("./images/img_MERKIT/merkit_all_black.svg");
    signs[10].load("./images/img_MERKIT/merkit_kuvien_taustakehys.svg");
    //signs[11].load("./images/img_TASK_04/task_04_img01.svg");

    /*
        SCOREBOARD IMAGES LOADING
    */
    ofLogNotice() << "Loading scoreboard graphics.";
    for (int i = 0; i < 28; i++)
    {
        scoreBoard.push_back(ofImage());
        stringstream ss;
        if (i < 10)
            ss << "./images/img_SCORES/scores_00" << i << ".png";
        else
            ss << "./images/img_SCORES/scores_0" << i << ".png";
        scoreBoard.back().loadImage(ss.str());
    }
    scoreBoardVisible = true;

    /*
        SPEECH BUBBLE IMAGES LOADING
    */
    ofLogNotice() << "Loading speech bubble graphics.";
    speechBubble.push_back(ofImage());
    speechBubble.push_back(ofImage());
    speechBubble.push_back(ofImage());
    speechBubble.push_back(ofImage());
    speechBubble.push_back(ofImage());
    speechBubble[SPEECH_BUBBLE_CORRECT_ANSWER].loadImage("./images/img_SPEECHBUBBLE/speechbubble_correct_answer.png");
    speechBubble[SPEECH_BUBBLE_WRONG_ANSWER].loadImage("./images/img_SPEECHBUBBLE/speechbubble_wrong_answer.png");
    speechBubble[SPEECH_BUBBLE_TASK].loadImage("./images/img_SPEECHBUBBLE/speechbubble_task.png");
    speechBubble[SPEECH_BUBBLE_TASK_1_ARROW_CORRECT].loadImage("./images/img_TASK_01/task_01_img02.png");
    //speechBubble[SPEECH_BUBBLE_TASK_1_ARROW_WRONG].loadImage("./images/img_TASK_01/task_01_img03.png");
    bubble = SPEECH_BUBBLE_CORRECT_ANSWER;
    speechBubbleVisible = true;

    ofLogNotice() << "Loading font.";
    font.loadFont("DINOT-Medium.otf",16,true,true,false,1,67);
    font.setLineHeight(19);

    ofLogNotice() << "Loading random graphics.";
    for (int i = 0; i < 27; i++)
    {
        graphics.push_back(ofImage());
    }
    graphics[TASK_2_NOTHING].loadImage("./images/img_TASK_02/task_02_img01a.png");
    graphics[TASK_2_GLOVES].loadImage("./images/img_TASK_02/task_02_img01b.png");
    graphics[TASK_2_GOGGLES].loadImage("./images/img_TASK_02/task_02_img01c.png");
    graphics[TASK_2_GLOVES_GOGGLES].loadImage("./images/img_TASK_02/task_02_img01d.png");
    graphics[TASK_2_EARMUFFS].loadImage("./images/img_TASK_02/task_02_img01e.png");
    graphics[TASK_2_EARMUFFS_GLOVES].loadImage("./images/img_TASK_02/task_02_img01f.png");
    graphics[TASK_2_EARMUFFS_GOGGLES].loadImage("./images/img_TASK_02/task_02_img01g.png");
    graphics[TASK_3_ACETONE].loadImage("./images/img_TASK_03/task_03_img01.png");

    graphics[GRAPHICS_HNO3].loadImage("./images/img_TASK_06/task_06_moveable.png");
    graphics[GRAPHICS_HNO3_OK].loadImage("./images/img_TASK_06/task_06_img02.png");
    graphics[GRAPHICS_HNO3_ARROWS].loadImage("./images/img_TASK_06/task_06_img03.png");
    graphics[GRAPHICS_QUESTION].loadImage("./images/img_TASK_06/task_06_img04.png");
    graphics[GRAPHICS_RED_NOT].loadImage("./images/img_TASK_06/task_06_img01.png");

    graphics[GRAPHICS_PAINT].loadImage("./images/img_TASK_07/TASK_07_moveable.png");
    graphics[GRAPHICS_PAINT_ARROWS].loadImage("./images/img_TASK_07/TASK_07_img10.png");
    graphics[GRAPHICS_TOILET_TERROR].loadImage("./images/img_TASK_07/TASK_07_img04.png");
    graphics[GRAPHICS_DOCK_TERROR].loadImage("./images/img_TASK_07/TASK_07_img05.png");
    graphics[GRAPHICS_PAINT_2_TOILET].loadImage("./images/img_TASK_07/TASK_07_img01b.png");
    graphics[GRAPHICS_PAINT_2_WATER].loadImage("./images/img_TASK_07/TASK_07_img02b.png");
    graphics[GRAPHICS_PAINT_2_CONTAINER].loadImage("./images/img_TASK_07/TASK_07_moveable.png");
    graphics[GRAPHICS_RED_NOT_TINY].loadImage("./images/img_TASK_07/TASK_07_img07.png");

    graphics[GRAPHICS_BLANK_LABEL].loadImage("./images/img_TASK_08/TASK_08_img04.png");
    graphics[GRAPHICS_STAR].loadImage("./images/img_TASK_08/TASK_08_moveable.png");
    graphics[GRAPHICS_H2O].loadImage("./images/img_TASK_08/TASK_08_img06.png");
    graphics[GRAPHICS_H2O_ARROWS].loadImage("./images/img_TASK_08/TASK_08_img05.png");
    graphics[GRAPHICS_H2O_QUESTION].loadImage("./images/img_TASK_08/TASK_08_img03.png");

    graphics[GRAPHICS_REMOVE_OBJECTS].loadImage("./images/img_INFO/info_remove_objects.png");

//    midiIn = new ofxMidiIn("varoitusmerkkipeli");
//    midiIn->openVirtualPort("in");
//    midiOut = new ofxMidiOut("varoitusmerkkipeli");
//    midiOut->openVirtualPort("out");
//    midiIn->addListener(this);

    ofLogNotice() << "Loading audio clips.";
    for (int i = 0; i < MAX_SOUNDS; i++)
    {
        sounds.push_back(ofSoundPlayer());
    }
    sounds[SOUNDS_CORRECT].loadSound("./sounds/aanimerkki_positiivinen.ogg");
    sounds[SOUNDS_FAIL].loadSound("./sounds/aanimerkki_negatiivinen.ogg");
    sounds[SOUNDS_CUTSCENE_2].loadSound("./sounds/framestory_1.ogg");
    sounds[SOUNDS_CUTSCENE_3].loadSound("./sounds/framestory_2.ogg");
    sounds[SOUNDS_CUTSCENE_4].loadSound("./sounds/framestory_3.ogg");
    sounds[SOUNDS_CUTSCENE_5].loadSound("./sounds/framestory_last.ogg");
    sounds[SOUNDS_TASK_3_FIRE].loadSound("./sounds/task_3_tuli.ogg");
    sounds[SOUNDS_TASK_2_ALARM].loadSound("./sounds/task_2_halytys.ogg");
    sounds[SOUNDS_TASK_2_ALARM].setLoop(true);
    sounds[SOUNDS_TASK_4_EXPLOSION].loadSound("./sounds/task_4_rajahdys.ogg");
    sounds[SOUNDS_TASK_4_FACTORY].loadSound("./sounds/task_4_tehdas.ogg");
    sounds[SOUNDS_TASK_4_FACTORY].setLoop(true);
    sounds[SOUNDS_TASK_5_BOTTLE].loadSound("./sounds/task_5_painepullo.ogg");
    sounds[SOUNDS_TASK_5_BOTTLE].setLoop(true);
    sounds[SOUNDS_TASK_7_NATURA].loadSound("./sounds/task_7_luontomaisema.ogg");
    sounds[SOUNDS_TASK_7_NATURA].setLoop(true);
    sounds[SOUNDS_TASK_9_ATTIC].loadSound("./sounds/task_9_vintti.ogg");
    sounds[SOUNDS_TASK_9_ATTIC].setLoop(true);
    sounds[SOUNDS_SNAP].loadSound("./sounds/snap.ogg");
    sounds[SOUNDS_CUTSCENE_1].loadSound("./sounds/gamestart_tuuli.ogg");

    ofLogNotice() << "SETUP finished!";

    newGame();

}

//--------------------------------------------------------------
void testApp::update()
{

    ofBackground(0,0,0);

    if (ofGetElapsedTimeMillis() - lastActionOnTable > 150000) {
        newGame();
    }

    if (taskState == TASK_STATE_JUDGEMENT_DELAY)
    {
        if (ofGetElapsedTimeMillis() - advanceTimer > 1000)
        {
            if (answerWasCorrect)
                announceWIN();
            else
                announceFAIL();
        }
        return;
    }

    kinect.update();

    if (kinect.isFrameNew())
    {
        colorImg.setFromPixels(kinect.getPixels(), 640,480);
        grayImage = colorImg;


        depthImage.setFromPixels(kinect.getDepthPixels(), kinect.width, kinect.height);
        depthStart = depthImage;

        if (depthFilterOn) {
            for (int i = 0; i < ROI_H; i++) {
                for (int j = 0; j < ROI_W; j++) {
                        int p = kinect.getDepthPixels()[(i + ROI_Y)*640 + j + ROI_X];
                        if (p > 0 && p < 250)
                            grayImage.getPixels()[i*ROI_W + j] = 0;
                }
            }
        }

        shouldIStart = 0;
        for (int i = 0; i < DEPTH_ROI_H*DEPTH_ROI_W; i++)
        {
            shouldIStart += depthStart.getPixels()[i];
        }
        shouldIStart /= DEPTH_ROI_H*DEPTH_ROI_W;

        if (shouldIStart >= 250) {
            startHysteresis ++;
            if (startHysteresis > 4) {
                startHysteresis = 10;
                if (task > 0) {
                    newGame();
                    lockAnswer(true);
                } else if (taskState == TASK_STATE_TASK_GIVEN) {
                    lockAnswer(true);
                }
            }
        } else {
            startHysteresis --;
            if (startHysteresis < 0)
                startHysteresis = 0;
        }

        grayImage.threshold(threshold);
        fidfinder.findFiducials( grayImage );

        /* hysteresis counter */
        for (int i = 0; i < 12; i++)
        {
            if (itemsOnTable[i]==1) {
                ofLogNotice() << "ITEM: " << i << " OFF TABLE!" << endl;
                lastActionOnTable = ofGetElapsedTimeMillis();
            }
            itemsOnTable[i] = max(0,itemsOnTable[i]-1);

        }

        for (list<ofxFiducial>::iterator fiducial = fidfinder.fiducialsList.begin(); fiducial != fidfinder.fiducialsList.end(); fiducial++)
        {
            if (fiducial->getId() < 12)
            {
                if (fiducial->getX() > 0 && fiducial->getY() > 0)
                {

                    depthImage.setROI(0,0,640,480);

                    //cout << "F: " << fiducial->getId() << " x: " << (int)fiducial->getX()+ROI_X << " Y: " << (int)fiducial->getY()+ROI_Y << " korkeus: " << (int)depthImage.getPixels()[(int)((fiducial->getY()+ROI_Y)*640 + fiducial->getX()+ROI_X)] << endl;
                    //cout << "F: " << fiducial->getId() << " korkeus: " << depthImage.getPixels()[32] << endl;

                    depthImage.setROI(DEPTH_ROI_X,DEPTH_ROI_Y,DEPTH_ROI_W,DEPTH_ROI_H);

                    /* We are here if same marker has been detected in two frames */

                    if (itemsOnTable[fiducial->getId()] == 0)
                    {
                        lastActionOnTable = ofGetElapsedTimeMillis();
                        ofLogNotice() << "ITEM: " << fiducial->getId() << " ON TABLE!";
                        postMouseX = fiducial->getX();
                        postMouseY = fiducial->getY();
                        postFidX = fiducial->getX();
                        postFidY = fiducial->getY();
                        lastItemOnTable = fiducial->getId();

                        if (!draggin && ((task == 6 && fiducial->getId()==ITEM_HNO3)
                            || (task == 7 && fiducial->getId()==ITEM_PAINT)
                            || (task == 8 && fiducial->getId()==ITEM_STAR)))
                        {
                            lockAnswer(true);
                        }
                    }
                    if (postFidX != fiducial->getX() || postFidY != fiducial->getY())
                    {
                        if (draggin && taskState == TASK_STATE_TASK_GIVEN && !rescan)
                        {
                            cursorX -= (postFidY - fiducial->getY())*10;
                            if (task == 7 || task == 8) {
                                cursorY -= (postFidX - fiducial->getX())*25;
                            } else {
                                cursorY -= (postFidX - fiducial->getX())*20;
                            }
                        }
                        postFidX = fiducial->getX();
                        postFidY = fiducial->getY();
                        rescan = false;

                        switch (task)
                        {
                        case 6:
                            if (!projection(-356,13,0,0,cursorX,cursorY,&cursorX,&cursorY,false,true, false))
                                if (!projection(268,210,0,0,cursorX,cursorY,&cursorX,&cursorY,false,true,true))
                                    if (!projection(268,210,28,400,cursorX,cursorY,&cursorX,&cursorY,true,true,true))
                                        projection(-356,13,28,400,cursorX,cursorY,&cursorX,&cursorY,true,true,true);
                            break;
                        case 7:
                            if (!projection(-150,-222,-584,-332,cursorX,cursorY,&cursorX,&cursorY,false,false,true))
                                if (!projection(0,0,-150,-222,cursorX,cursorY,&cursorX,&cursorY,false,false,true))
                                    if (!projection(0,0,-492,137,cursorX,cursorY,&cursorX,&cursorY,true,true, true))
                                        projection(-584,-332,-492,137,cursorX,cursorY,&cursorX,&cursorY,true,true,true);
                            break;
                        case 8:
                            if (!projection(-535,472,0,0,cursorX,cursorY,&cursorX,&cursorY,false,true,true))
                                if (!projection(0,0,-49,602,cursorX,cursorY,&cursorX,&cursorY,true,true,true))
                                    if (!projection(-535,472,-293,596,cursorX,cursorY,&cursorX,&cursorY,true,true,false));
                                        projection(-293,596,-49,602,cursorX,cursorY,&cursorX,&cursorY,true,true,true);

                            break;
                        }
                        //cout << "x:" << (cursorX) << " y:" << (cursorY) <<  "mx:" << fiducial->getX() << " my:" << fiducial->getY() << endl;
                    }
                    itemsOnTable[fiducial->getId()] = 20;
                }
            }
        }
        // }


        if (taskState == TASK_STATE_TASK_GIVEN)
        {
            switch (task)
            {
            case 1:
            {
                if (itemsOnTable[ITEM_SHOUT]) lockAnswer(true);//correctAnswer();
                else if (anyItemOnTable()) lockAnswer(false); //incorrectAnswer();
                break;
            }
            case 2:
            {
                if (itemsOnTable[ITEM_GLOVES] && !slotsGuessed[SLOT_TASK_2_GLOVES])
                {
                    slotsGuessed[SLOT_TASK_2_GLOVES] = true;
                    lockAnswer(true);//correctAnswer();
                }
                else if (itemsOnTable[ITEM_GLASSES] && !slotsGuessed[SLOT_TASK_2_GOGGLES])
                {
                    slotsGuessed[SLOT_TASK_2_GOGGLES] = true;
                    lockAnswer(true);//correctAnswer();
                }
                else
                {
                    bool b = false;
                    for (int i = 0; i < 12; i++)
                    {
                        if (i != ITEM_GLASSES && i != ITEM_GLOVES && itemsOnTable[i])
                            b = true;
                    }
                    if (b)
                        lockAnswer(false);//incorrectAnswer();
                }
                break;
            }
            case 3:
            {
                if (itemsOnTable[ITEM_ACETONE]) lockAnswer(true);
                else if (anyItemOnTable()) lockAnswer(false);
                break;
            }
            case 4:
            {
                if (itemsOnTable[ITEM_FLASHLIGHT]) lockAnswer(true);
                else if (anyItemOnTable()) lockAnswer(false);
                break;
            }
            case 5:
            {
                if (itemsOnTable[ITEM_SODASTREAM]) lockAnswer(true);
                else if (anyItemOnTable()) lockAnswer(false);
                break;
            }
            case 6:
            {
                if (itemsOnTable[ITEM_HNO3])
                {
                    if (cursorX > 30 || cursorX < -30 || cursorY > 30)
                        cursorVisible = false;
                    if ( cursorX > 267)
                    {
                        lockAnswer(true);
                    }
                    else if (cursorX < -355 && !slotsGuessed[SLOT_TASK_6_1])
                    {
                        slotsGuessed[SLOT_TASK_6_1] = true;
                        lastSlotGuessed = SLOT_TASK_6_1;
                        lockAnswer(false);
                    }
                    else if (cursorY > 399 && !slotsGuessed[SLOT_TASK_6_2])
                    {
                        slotsGuessed[SLOT_TASK_6_2] = true;
                        lastSlotGuessed = SLOT_TASK_6_2;
                        lockAnswer(false);
                    }
                }
                else if (anyItemOnTable())
                {
                    lockAnswer(false);
                }
                break;
            }
            case 7:
            {
                if (itemsOnTable[ITEM_PAINT])
                {
                    if (cursorX < -30)
                        cursorVisible = false; // 584,332
                    if ( cursorX > -151 && cursorY < -220 )
                    {
                        paintGoingTo = PAINT_2_CONTAINER;
                        lockAnswer(true);
                    }
                    else if (cursorX < -583 && cursorY < -330 && !slotsGuessed[SLOT_TASK_7_1])
                    {
                        paintGoingTo = PAINT_2_TOILET;
                        slotsGuessed[SLOT_TASK_7_1] = true;
                        lockAnswer(false);
                    }
                    else if (cursorX < -491 && cursorY  > 136 && !slotsGuessed[SLOT_TASK_7_2])
                    {
                        paintGoingTo = PAINT_2_WATER;
                        slotsGuessed[SLOT_TASK_7_2] = true;
                        lockAnswer(false);
                    }
                    else
                    {
                        paintGoingTo = 666;
                    }
                }
                else if (anyItemOnTable())
                {
                    lockAnswer(false);
                }
                break;
            }
            case 8:
            {
                if (itemsOnTable[ITEM_STAR])
                {
                    if (cursorY > 300)
                        cursorVisible = false;

                    if ( cursorX < -534)
                    {
                        if ( !slotsGuessed[SLOT_TASK_8_1])
                        {
                            slotsGuessed[SLOT_TASK_8_1] = true;
                            if (slotsGuessed[SLOT_TASK_8_1] && slotsGuessed[SLOT_TASK_8_2] && slotsGuessed[SLOT_TASK_8_3])
                            {
                                slotsGuessed[SLOT_TASK_8_1] = false;
                                lockAnswer(true);
                            }
                            else
                            {
                                lockAnswer(false);
                            }
                        }
                    }
                    else if (cursorY > 595 && cursorX > - 294 && cursorX < -291 && !slotsGuessed[SLOT_TASK_8_2])
                    {

                        slotsGuessed[SLOT_TASK_8_2] = true;
                        if (slotsGuessed[SLOT_TASK_8_1] && slotsGuessed[SLOT_TASK_8_2] && slotsGuessed[SLOT_TASK_8_3])
                        {
                            slotsGuessed[SLOT_TASK_8_2] = false;
                            lockAnswer(true);
                        }
                        else
                        {
                            lockAnswer(false);
                        }
                    }
                    else if (cursorX > -49 && cursorY  > 598 && !slotsGuessed[SLOT_TASK_8_3])
                    {

                        slotsGuessed[SLOT_TASK_8_3] = true;
                        if (slotsGuessed[SLOT_TASK_8_1] && slotsGuessed[SLOT_TASK_8_2] && slotsGuessed[SLOT_TASK_8_3])
                        {
                            slotsGuessed[SLOT_TASK_8_3] = false;
                            lockAnswer(true);
                        }
                        else
                        {
                            lockAnswer(false);
                        }
                    }
                }
                else if (anyItemOnTable())
                {
                    lockAnswer(false);
                }
                break;
            }
            case 9:
            {
                if (itemsOnTable[ITEM_POISON]) lockAnswer(true);
                else if (anyItemOnTable()) lockAnswer(false);
                break;
            }
            }
        }
        else if (taskState == TASK_STATE_WRONG_ANSWER_GIVEN)
        {
            if (ofGetElapsedTimeMillis() - advanceTimer > MAX(textString.length() * DELAY_PER_LETTER, 3000)) //DELAY_WRONG_ANSWER)
            {
                taskState = TASK_STATE_PLEASE_CLEAR_THE_TABLE;
            }
        }
        else if (taskState == TASK_STATE_CORRECT_ANSWER_GIVEN)
        {
            if (ofGetElapsedTimeMillis() - advanceTimer > MAX(textString.length() * DELAY_PER_LETTER, 3000)) //DELAY_CORRECT_ANSWER)
            {
                fboUpdateRequested = true;
                if (task == 2 && (!slotsGuessed[SLOT_TASK_2_GLOVES] || !slotsGuessed[SLOT_TASK_2_GOGGLES]))
                {
                    initNewTask(task);
                }
                else
                {
                    /* We go to either a cutscene or head straight towards the next task */
                    if (task==3 || task==6 || task==8 )
                    {
                        switch (task)
                        {
                        case 3:
                            background.loadImage("./images/img_FRAMESTORY/framestory02_bg_earlyspring_1280x800.png");
                            sounds[SOUNDS_CUTSCENE_2].play();
                            break;
                        case 6:
                            if (sounds[SOUNDS_TASK_4_FACTORY].getIsPlaying());
                            sounds[SOUNDS_TASK_4_FACTORY].stop();
                            background.loadImage("./images/img_FRAMESTORY/framestory03_bg_spring_1280x800.png");
                            sounds[SOUNDS_CUTSCENE_3].play();
                            break;
                        case 8:
                            background.loadImage("./images/img_FRAMESTORY/framestory04_bg_summer_1280x800.png");
                            sounds[SOUNDS_CUTSCENE_4].play();
                            break;
                       /* case 9:
                            background.loadImage("./images/img_TASK_09/task_09d_bg_1280x800.png");
                            break;*/
                        }

                        taskState = TASK_STATE_CUT_SCENE;

                        stringstream ss;
                        ss << "./texts/fs_" << (task) << ".txt";
                        loadTextFromFile(ss.str());
                        advanceTimer = ofGetElapsedTimeMillis();


                    }
                    else
                    {
                        taskState = TASK_STATE_PLEASE_CLEAR_THE_TABLE;
                    }
                }
            }
            else if (task == 2)
            {
                if (itemsOnTable[ITEM_GLOVES] && !slotsGuessed[SLOT_TASK_2_GLOVES])
                {
                    slotsGuessed[SLOT_TASK_2_GLOVES] = true;
                    lockAnswer(true);
                }
                else if (itemsOnTable[ITEM_GLASSES] && !slotsGuessed[SLOT_TASK_2_GOGGLES])
                {
                    slotsGuessed[SLOT_TASK_2_GOGGLES] = true;
                    lockAnswer(true);
                }
            }


        }

        else if (taskState==TASK_STATE_CUT_SCENE)
        {
            if (ofGetElapsedTimeMillis() - advanceTimer > DELAY_CUT_SCENE)
            {
                taskState = TASK_STATE_PLEASE_CLEAR_THE_TABLE;
            }
        }

        if (taskState == TASK_STATE_PLEASE_CLEAR_THE_TABLE)
        {
            if (!anyItemOnTable())
            {
                if (taskCompleted[task])
                {
                    task++;
                }

                if (task==11)
                {
                    newGame();
                }
                else
                {
                    initNewTask(task);
                }
            }
            else if (task == 2)
            {
                bool b = true;
                for (int i = 0; i < 12; i++)
                {
                    if (i != ITEM_GLASSES && i != ITEM_GLOVES && itemsOnTable[i])
                        b = false;
                }
                if (b && !taskCompleted[task])
                    initNewTask(task);
            }
            else if (task == 6 || task == 7 || task == 8)
            {
                int item = 0;
                if (task==6) item = ITEM_HNO3;
                else if (task==7) item = ITEM_PAINT;
                else if (task==8) item = ITEM_STAR;
                bool b = true;
                for (int i = 0; i < 12; i++)
                {
                    if (i != item && itemsOnTable[i])
                        b = false;
                }
                if (b && !taskCompleted[task])
                    initNewTask(task);
            }
        }


        /* UPDATES NOT HAVING ANYTHING TO DO WITH TRACKING */



    }

    if (fboMode && fboUpdateRequested)
        updateFbo();

    return;

}

void testApp::updateStaticContent()
{
    ofSetHexColor(0xffffff);

    background.draw(0,0);

    // SIGN TABLE
    if (signTableVisible)
    {
        signTableBackground.draw(0,0);
        for (int i = 0; i < 9; i++)
        {
            ofPushMatrix();

            if (task==8 && itemsOnTable[ITEM_STAR] && draggin && i==7 && !taskCompleted[8])
                continue;

            if (task == i+1 && (!taskCompleted[task]))
            {
                ofTranslate(i*(1280-25*2-123)/8 + 25 , 11);
                ofPushMatrix();
                ofScale(123/signs[0].getWidth(),123/signs[0].getWidth());
            }
            else
            {
                ofTranslate(i*(1280-50*2-73)/8 + 50 , 35);
                ofPushMatrix();
                ofScale(73/signs[0].getWidth(),73/signs[0].getWidth());
            }
            if (taskCompleted[i+1] || task==i+1)
            {
                signs[10].draw();
                if (task!=1 || taskCompleted[i+1])
                    signs[i].draw();
                if (task==1 && taskCompleted[i+1])
                {
                    ofPushMatrix();
                    ofTranslate(1148,800);
                    ofScale(1.684931507,1.684931507);
                    signs[10].draw();
                    signs[0].draw();
                    ofPopMatrix();
                }
            }
            else
            {
                signs[9].draw();
            }
            ofPopMatrix();
            ofTranslate(i*142,0);
            ofPopMatrix();
        }
    }


    switch (task)
    {
    case 0:
    {
        if (taskState == TASK_STATE_TASK_GIVEN || taskState == TASK_STATE_JUDGEMENT_DELAY)
            return;
    }
    case 3:
    {
        if (taskState == TASK_STATE_TASK_GIVEN || taskState == TASK_STATE_JUDGEMENT_DELAY)
        {
            graphics[TASK_3_ACETONE].draw(745,600);
        }
        break;
    }
    case 6:
    {
        if (taskState == TASK_STATE_TASK_GIVEN || taskState == TASK_STATE_JUDGEMENT_DELAY || taskState == TASK_STATE_WRONG_ANSWER_GIVEN)
        {

            if (!slotsGuessed[SLOT_TASK_6_1])
                graphics[GRAPHICS_QUESTION].draw(418,256);
            if (!slotsGuessed[SLOT_TASK_6_2])
                graphics[GRAPHICS_QUESTION].draw(801,649);

            graphics[GRAPHICS_QUESTION].draw(1042,453);

            if (itemsOnTable[ITEM_HNO3] && draggin)
            {
                if (cursorVisible)
                {
                    graphics[GRAPHICS_HNO3_ARROWS].draw(622,256);
                }
                graphics[GRAPHICS_HNO3].draw(735 + cursorX,150 + cursorY);
            }

            if (slotsGuessed[SLOT_TASK_6_1])
                graphics[GRAPHICS_RED_NOT].draw(349,225);
            if (slotsGuessed[SLOT_TASK_6_2])
                graphics[GRAPHICS_RED_NOT].draw(733,608);

        }
        else if (taskState == TASK_STATE_CORRECT_ANSWER_GIVEN)
        {
            graphics[GRAPHICS_HNO3_OK].draw(957,366);
            graphics[GRAPHICS_RED_NOT].draw(349,225);
            graphics[GRAPHICS_RED_NOT].draw(733,608);
        }
        break;
    }
    case 7:
    {
        if (taskState == TASK_STATE_TASK_GIVEN || taskState == TASK_STATE_JUDGEMENT_DELAY)
        {
            if (!slotsGuessed[SLOT_TASK_7_1])
            {
                graphics[GRAPHICS_QUESTION].draw(566,216);
            }
            else
            {
                graphics[GRAPHICS_TOILET_TERROR].draw(369,309);
            }
            if (!slotsGuessed[SLOT_TASK_7_2])
            {
                graphics[GRAPHICS_QUESTION].draw(676,634);
            }
            else
            {
                graphics[GRAPHICS_DOCK_TERROR].draw(656,647);
            }
            graphics[GRAPHICS_QUESTION].draw(896,180);

            if (itemsOnTable[ITEM_PAINT] && draggin)
            {
                //graphics[GRAPHICS_PAINT_2_TOILET].draw(473,134); 584,332
                switch (paintGoingTo)
                {
                case PAINT_2_TOILET:
                    graphics[GRAPHICS_PAINT_2_TOILET].draw(1056 + cursorX,466 + cursorY);
                    break;
                case PAINT_2_WATER:
                    graphics[GRAPHICS_PAINT_2_WATER].draw(1056 + cursorX,466 + cursorY);
                    break;
                default:
                    graphics[GRAPHICS_PAINT_2_CONTAINER].draw(1056 + cursorX,466 + cursorY);
                    break;
                }

                if (cursorVisible)
                {
                    graphics[GRAPHICS_PAINT_ARROWS].draw(968,375);
                }

                if (slotsGuessed[SLOT_TASK_7_1])
                {
                    graphics[GRAPHICS_RED_NOT_TINY].draw(475,141);
                }
                if (slotsGuessed[SLOT_TASK_7_2])
                {
                    graphics[GRAPHICS_RED_NOT_TINY].draw(558,614);
                }
            }

        }
        else if (taskState == TASK_STATE_WRONG_ANSWER_GIVEN)
        {
            if (itemsOnTable[ITEM_PAINT] && draggin) {
                switch (paintGoingTo)
                {
                case PAINT_2_TOILET:
                    graphics[GRAPHICS_PAINT_2_TOILET].draw(1056 + cursorX,466 + cursorY);
                    break;
                case PAINT_2_WATER:
                    graphics[GRAPHICS_PAINT_2_WATER].draw(1056 + cursorX,466 + cursorY);
                    break;
                default:
                    graphics[GRAPHICS_PAINT_2_CONTAINER].draw(1056 + cursorX,466 + cursorY);
                    break;
                }
            }
            //graphics[GRAPHICS_PAINT_ARROWS].draw(968 + cursorX,375 + cursorY);
            if (!slotsGuessed[SLOT_TASK_7_1])
            {
                graphics[GRAPHICS_QUESTION].draw(566,216);

            }
            else
            {
                graphics[GRAPHICS_TOILET_TERROR].draw(369,309);
                graphics[GRAPHICS_RED_NOT_TINY].draw(475,141);
            }
            if (!slotsGuessed[SLOT_TASK_7_2])
            {
                graphics[GRAPHICS_QUESTION].draw(676,634);
            }
            else
            {
                graphics[GRAPHICS_DOCK_TERROR].draw(656,647);
                graphics[GRAPHICS_RED_NOT_TINY].draw(558,614);
            }
            graphics[GRAPHICS_QUESTION].draw(896,180);

        }
        break;
    }
    case 8:
    {
        if (taskState != TASK_STATE_WRONG_ANSWER_GIVEN && taskState != TASK_STATE_TASK_GIVEN && taskState != TASK_STATE_CORRECT_ANSWER_GIVEN && taskState != TASK_STATE_JUDGEMENT_DELAY)
            break;
        if (cursorVisible)
            graphics[GRAPHICS_H2O_ARROWS].draw(542,131);

        if (slotsGuessed[SLOT_TASK_8_1])
        {
            graphics[GRAPHICS_H2O].draw(465,362);
        }
        else if (taskState!=TASK_STATE_CORRECT_ANSWER_GIVEN)
        {
            graphics[GRAPHICS_H2O_QUESTION].draw(495,360);
            graphics[GRAPHICS_BLANK_LABEL].draw(457,479);
        }
        else
        {
            graphics[GRAPHICS_STAR].draw(457,479);
        }

        if (slotsGuessed[SLOT_TASK_8_2])
        {
            graphics[GRAPHICS_H2O].draw(703,513);
        }
        else if (taskState!=TASK_STATE_CORRECT_ANSWER_GIVEN)
        {
            graphics[GRAPHICS_H2O_QUESTION].draw(735,510);
            graphics[GRAPHICS_BLANK_LABEL].draw(698,604);
        }
        else
        {
            graphics[GRAPHICS_STAR].draw(698,604);
        }

        if (slotsGuessed[SLOT_TASK_8_3])
        {
            graphics[GRAPHICS_H2O].draw(949,492);
        }
        else if (taskState!=TASK_STATE_CORRECT_ANSWER_GIVEN)
        {
            graphics[GRAPHICS_H2O_QUESTION].draw(982,489);
            graphics[GRAPHICS_BLANK_LABEL].draw(941,609);
        }
        else
        {
            graphics[GRAPHICS_STAR].draw(941,609);
        }

        if (itemsOnTable[ITEM_STAR] && !taskCompleted[task])
        {
            graphics[GRAPHICS_STAR].draw(991 + cursorX,7 + cursorY);
        }
        break;
    }
    }

    // SIGN TABLE
    /* if (signTableVisible)
     {
         signTableBackground.draw(0,0);
         for (int i = 0; i < 9; i++)
         {
             ofPushMatrix();

             if (task == i+1)
             {
                 ofTranslate(i*(1280-25*2-123)/8 + 25 , 11);
                 ofPushMatrix();
                 ofScale(123/signs[0].getWidth(),123/signs[0].getWidth());
             }
             else
             {
                 ofTranslate(i*(1280-50*2-73)/8 + 50 , 35);
                 ofPushMatrix();
                 ofScale(73/signs[0].getWidth(),73/signs[0].getWidth());
             }
             if (taskCompleted[i+1] || task==i+1)
             {
                 signs[10].draw();
                 if (task!=1 || taskCompleted[i+1])
                     signs[i].draw();
                 if (task==1 && taskCompleted[i+1]) {
                     ofPushMatrix();
                     ofTranslate(721,517);
                     signs[10].draw();
                     signs[0].draw();
                     ofPopMatrix();
                 }
             }
             else
             {
                 signs[9].draw();
             }
             ofPopMatrix();
             ofTranslate(i*142,0);
             ofPopMatrix();
         }
     }*/

    // SPEECH BUBBLE
    if (speechBubbleVisible)
    {
        if (taskState == TASK_STATE_TASK_GIVEN || taskState == TASK_STATE_JUDGEMENT_DELAY)
        {
            speechBubble[SPEECH_BUBBLE_TASK].draw(0,140);
            if (task==1)
            {
                speechBubble[SPEECH_BUBBLE_TASK_1_ARROW_CORRECT].draw(360,230);
            }
        }
        else if (taskState == TASK_STATE_CORRECT_ANSWER_GIVEN ||
                 taskState == TASK_STATE_CUT_SCENE ||
                 (taskState == TASK_STATE_PLEASE_CLEAR_THE_TABLE && taskCompleted[task]))
        {
            speechBubble[SPEECH_BUBBLE_CORRECT_ANSWER].draw(0,140);

        }
        else if (taskState == TASK_STATE_WRONG_ANSWER_GIVEN ||
                 (taskState == TASK_STATE_PLEASE_CLEAR_THE_TABLE && !taskCompleted[task]))
        {
            speechBubble[SPEECH_BUBBLE_WRONG_ANSWER].draw(0,140);
        }

        ofSetHexColor(0x000000);
        font.drawString(textString,42,310);
        ofSetHexColor(0xffffff);
    }

    // SCORE BOARD
    if (scoreBoardVisible)
    {
        scoreBoard[points].draw(1145,140);
    }

    if (task==2)
    {
        if (slotsGuessed[SLOT_TASK_2_GLOVES])
        {
            if (slotsGuessed[SLOT_TASK_2_GOGGLES])
            {
                graphics[TASK_2_GLOVES_GOGGLES].draw(370,233);
            }
            else if (itemsOnTable[ITEM_EARMUFFS])
            {
                graphics[TASK_2_EARMUFFS_GLOVES].draw(370,233);
            }
            else
            {
                graphics[TASK_2_GLOVES].draw(370,233);
            }
        }
        else if (slotsGuessed[SLOT_TASK_2_GOGGLES])
        {
            if (itemsOnTable[ITEM_EARMUFFS])
            {
                graphics[TASK_2_EARMUFFS_GOGGLES].draw(370,233);
            }
            else
            {
                graphics[TASK_2_GOGGLES].draw(370,233);
            }
        }
        else if (itemsOnTable[ITEM_EARMUFFS])
        {
            graphics[TASK_2_EARMUFFS].draw(370,233);
        }
        else
        {
            graphics[TASK_2_NOTHING].draw(370,233);
        }
    }


}

//--------------------------------------------------------------
void testApp::draw()
{
    ofSetHexColor(0xffffff);

    if (!fboMode)
        updateStaticContent();
    else
        staticContent.draw(0,0);

    // if (taskState != TASK_STATE_PLEASE_CLEAR_THE_TABLE)
    /*    background.draw(0,0);

        switch (task)
        {
        case 0:
            if (taskState == TASK_STATE_TASK_GIVEN)
                return;
        case 3:
            if (taskState == TASK_STATE_TASK_GIVEN || taskState == TASK_STATE_JUDGEMENT_DELAY)
            {
                graphics[TASK_3_ACETONE].draw(745,600);
            }
            break;
        case 6:
            if (taskState == TASK_STATE_TASK_GIVEN || taskState == TASK_STATE_JUDGEMENT_DELAY)
            {

                if (!slotsGuessed[SLOT_TASK_6_1])
                    graphics[GRAPHICS_QUESTION].draw(418,256);
                if (!slotsGuessed[SLOT_TASK_6_2])
                    graphics[GRAPHICS_QUESTION].draw(801,649);

                graphics[GRAPHICS_QUESTION].draw(1042,453);

                if (itemsOnTable[ITEM_HNO3])
                {
                    if (cursorVisible)
                    {
                        graphics[GRAPHICS_HNO3_ARROWS].draw(622,256);
                    }
                    graphics[GRAPHICS_HNO3].draw(735 + cursorX,150 + cursorY);
                }

                if (slotsGuessed[SLOT_TASK_6_1])
                    graphics[GRAPHICS_RED_NOT].draw(349,225);
                if (slotsGuessed[SLOT_TASK_6_2])
                    graphics[GRAPHICS_RED_NOT].draw(733,608);

            }
            else if (taskState == TASK_STATE_CORRECT_ANSWER_GIVEN)
            {
                graphics[GRAPHICS_HNO3_OK].draw(958,356);
                graphics[GRAPHICS_RED_NOT].draw(349,225);
                graphics[GRAPHICS_RED_NOT].draw(733,608);
            }
            else if (taskState == TASK_STATE_WRONG_ANSWER_GIVEN)
            {

                graphics[GRAPHICS_HNO3].draw(735 + cursorX,150 + cursorY);

                if (!slotsGuessed[SLOT_TASK_6_1])
                {
                    graphics[GRAPHICS_QUESTION].draw(418,256);
                }
                else
                {
                    graphics[GRAPHICS_RED_NOT].draw(349,225);
                }
                if (!slotsGuessed[SLOT_TASK_6_2])
                {
                    graphics[GRAPHICS_QUESTION].draw(801,649);
                }
                else
                {
                    graphics[GRAPHICS_RED_NOT].draw(733,608);
                }
                graphics[GRAPHICS_QUESTION].draw(1042,453);
            }
            break;
        case 7:
            if (taskState == TASK_STATE_TASK_GIVEN || taskState == TASK_STATE_JUDGEMENT_DELAY)
            {
                if (!slotsGuessed[SLOT_TASK_7_1])
                {
                    graphics[GRAPHICS_QUESTION].draw(554,180);
                }
                else
                {
                    graphics[GRAPHICS_TOILET_TERROR].draw(369,309);
                }
                if (!slotsGuessed[SLOT_TASK_7_2])
                {
                    graphics[GRAPHICS_QUESTION].draw(690,630);
                }
                else
                {
                    graphics[GRAPHICS_DOCK_TERROR].draw(656,647);
                }
                graphics[GRAPHICS_QUESTION].draw(900,213);

                if (itemsOnTable[ITEM_PAINT])
                {
                    switch (paintGoingTo)
                    {
                    case PAINT_2_TOILET:
                        graphics[GRAPHICS_PAINT_2_TOILET].draw(1056 + cursorX,466 + cursorY);
                        break;
                    case PAINT_2_WATER:
                        graphics[GRAPHICS_PAINT_2_WATER].draw(1056 + cursorX,466 + cursorY);
                        break;
                    default:
                        graphics[GRAPHICS_PAINT_2_CONTAINER].draw(1056 + cursorX,466 + cursorY);
                        break;
                    }

                    //graphics[GRAPHICS_PAINT].draw(mouseX,mouseY);
                    if (cursorVisible)
                    {
                        graphics[GRAPHICS_PAINT_ARROWS].draw(968,375);
                        //graphics[GRAPHICS_PAINT_ARROWS].draw(mouseX,mouseY);
                    }

                    //graphics[GRAPHICS_PAINT_ARROWS].draw(968 + cursorX,375 + cursorY);
                    if (slotsGuessed[SLOT_TASK_7_1])
                    {
                        graphics[GRAPHICS_RED_NOT_TINY].draw(471,141);
                    }
                    if (slotsGuessed[SLOT_TASK_7_2])
                    {
                        graphics[GRAPHICS_RED_NOT_TINY].draw(558,616);
                    }
                }

            }
            else if (taskState == TASK_STATE_WRONG_ANSWER_GIVEN)
            {
                //graphics[GRAPHICS_PAINT_ARROWS].draw(968 + cursorX,375 + cursorY);
                if (!slotsGuessed[SLOT_TASK_7_1])
                {
                    graphics[GRAPHICS_QUESTION].draw(554,180);

                }
                else
                {
                    graphics[GRAPHICS_TOILET_TERROR].draw(369,309);
                    graphics[GRAPHICS_RED_NOT_TINY].draw(471,141);
                }
                if (!slotsGuessed[SLOT_TASK_7_2])
                {
                    graphics[GRAPHICS_QUESTION].draw(690,630);
                }
                else
                {
                    graphics[GRAPHICS_DOCK_TERROR].draw(656,647);
                    graphics[GRAPHICS_RED_NOT_TINY].draw(558,616);
                }
                graphics[GRAPHICS_QUESTION].draw(900,213);
            }
            break;
        case 8:
        {
            if (taskState != TASK_STATE_WRONG_ANSWER_GIVEN && taskState != TASK_STATE_TASK_GIVEN && taskState != TASK_STATE_CORRECT_ANSWER_GIVEN)
                break;
            if (cursorVisible)
                graphics[GRAPHICS_H2O_ARROWS].draw(542,131);

            if (slotsGuessed[SLOT_TASK_8_1])
            {
                graphics[GRAPHICS_H2O].draw(464,497);
            }
            else if (taskState!=TASK_STATE_CORRECT_ANSWER_GIVEN)
            {
                graphics[GRAPHICS_H2O_QUESTION].draw(494,347);
                graphics[GRAPHICS_BLANK_LABEL].draw(456,467);
            }
            else
            {
                graphics[GRAPHICS_STAR].draw(456,467);
            }

            if (slotsGuessed[SLOT_TASK_8_2])
            {
                graphics[GRAPHICS_H2O].draw(706,562);
            }
            else if (taskState!=TASK_STATE_CORRECT_ANSWER_GIVEN)
            {
                graphics[GRAPHICS_H2O_QUESTION].draw(734,428);
                graphics[GRAPHICS_BLANK_LABEL].draw(697,534);
            }
            else
            {
                graphics[GRAPHICS_STAR].draw(697,534);
            }

            if (slotsGuessed[SLOT_TASK_8_3])
            {
                graphics[GRAPHICS_H2O].draw(949,637);
            }
            else if (taskState!=TASK_STATE_CORRECT_ANSWER_GIVEN)
            {
                graphics[GRAPHICS_H2O_QUESTION].draw(978,519);
                graphics[GRAPHICS_BLANK_LABEL].draw(942,607);
            }
            else
            {
                graphics[GRAPHICS_STAR].draw(942,607);
            }

        }

        if (itemsOnTable[ITEM_STAR])
        {
            graphics[GRAPHICS_STAR].draw(991 + cursorX,7 + cursorY);
        }
        break;
        }

        // SIGN TABLE
        if (signTableVisible)
        {
            signTableBackground.draw(0,0);
            for (int i = 0; i < 9; i++)
            {
                ofPushMatrix();

                if (task == i+1)
                {
                    ofTranslate(i*(1280-25*2-123)/8 + 25 , 11);
                    ofPushMatrix();
                    ofScale(123/signs[0].getWidth(),123/signs[0].getWidth());
                }
                else
                {
                    ofTranslate(i*(1280-50*2-73)/8 + 50 , 35);
                    ofPushMatrix();
                    ofScale(73/signs[0].getWidth(),73/signs[0].getWidth());
                }
                if (taskCompleted[i+1] || task==i+1)
                {
                    signs[10].draw();
                    if (task!=1 || taskCompleted[i+1])
                        signs[i].draw();
                }
                else
                {
                    signs[9].draw();
                }
                ofPopMatrix();
                ofTranslate(i*142,0);
                ofPopMatrix();
            }
        }

        */

    if (taskState==TASK_STATE_PLEASE_CLEAR_THE_TABLE)
        graphics[GRAPHICS_REMOVE_OBJECTS].draw(230,140);

    if (debugGraphics)
    {
        colorImg.draw(640,0);
        grayImage.draw(ROI_X+640,ROI_Y);
        //grayImage.draw(0,0);
        depthImage.draw(0,0);
        ofNoFill();
        ofSetHexColor(0x00ff00);
        ofRect(DEPTH_ROI_X,DEPTH_ROI_Y,DEPTH_ROI_W,DEPTH_ROI_H);
        ofSetHexColor(0x00fff0);
        char fpsStr[255]; // an array of chars
        sprintf(fpsStr, "%f %i %i %i", ofGetFrameRate(), shouldIStart, textString.length(), textString.length()*DELAY_PER_LETTER);
        font.drawString(fpsStr,70,730);

        /*for (list<ofxFiducial>::iterator fiducial = fidfinder.fiducialsList.begin(); fiducial != fidfinder.fiducialsList.end(); fiducial++) {
            fiducial->draw( 20, 20 );//draw fiducial
            fiducial->drawCorners( 20, 20 );//draw corners
            ofRect(fiducial->getX()+ROI_X,fiducial->getY()+ROI_Y, 20, 20);
        }*/
        ofSetHexColor(0xffffff);
    }
}

void testApp::silence()
{
    for (int i = 0; i < MAX_SOUNDS; i++)
    {
        if (sounds[i].getIsPlaying())
            sounds[i].stop();
    }
}

void testApp::initNewTask(int t)
{
    lastActionOnTable = ofGetElapsedTimeMillis();
    taskState = TASK_STATE_TASK_GIVEN;
    advanceTimer = ofGetElapsedTimeMillis();
    switch (t)
    {
    case 0:
    {
        background.loadImage("./images/img_START/START_bg_1280x800_flat.png");
        break;
    }
    case 1:
    {
        if (sounds[SOUNDS_CUTSCENE_1].getIsPlaying())
            sounds[SOUNDS_CUTSCENE_1].stop();
        background.loadImage("./images/img_TASK_01/task_01_bg_1280x800.png");
        break;
    }
    case 2:
    {
        if (!sounds[SOUNDS_TASK_2_ALARM].getIsPlaying())
            sounds[SOUNDS_TASK_2_ALARM].play();
        background.loadImage("./images/img_TASK_02/task_02_bg_1280x800.png");
        break;
    }
    case 3:
    {
        if (sounds[SOUNDS_TASK_2_ALARM].getIsPlaying())
            sounds[SOUNDS_TASK_2_ALARM].stop();
        background.loadImage("./images/img_TASK_03/task_03a_bg_1280x800.png");
        break;
    }
    case 4:
    {
        if (sounds[SOUNDS_CUTSCENE_2].getIsPlaying())
            sounds[SOUNDS_CUTSCENE_2].stop();
        if (!sounds[SOUNDS_TASK_4_FACTORY].getIsPlaying())
            sounds[SOUNDS_TASK_4_FACTORY].play();
        background.loadImage("./images/img_TASK_04/TASK_04a_bg_1280x800.png");
        break;
    }
    case 5:
    {
        if (sounds[SOUNDS_TASK_4_FACTORY].getIsPlaying())
            sounds[SOUNDS_TASK_4_FACTORY].stop();
        if (!sounds[SOUNDS_TASK_5_BOTTLE].getIsPlaying())
            sounds[SOUNDS_TASK_5_BOTTLE].play();
        background.loadImage("./images/img_TASK_05/task_05a_bg_1280x800.png");
        break;
    }
    case 6:
    {
        if (sounds[SOUNDS_CUTSCENE_3].getIsPlaying())
            sounds[SOUNDS_CUTSCENE_3].stop();
        if (!sounds[SOUNDS_TASK_4_FACTORY].getIsPlaying())
            sounds[SOUNDS_TASK_4_FACTORY].play();
        background.loadImage("./images/img_TASK_06/task_06_bg_1280x800.png");
        //cursorVisible = true;
        break;
    }
    case 7:
    {
        if (sounds[SOUNDS_CUTSCENE_3].getIsPlaying())
            sounds[SOUNDS_CUTSCENE_3].stop();
        if (sounds[SOUNDS_TASK_4_FACTORY].getIsPlaying())
            sounds[SOUNDS_TASK_4_FACTORY].stop();
        if (!sounds[SOUNDS_TASK_7_NATURA].getIsPlaying())
            sounds[SOUNDS_TASK_7_NATURA].play();
        background.loadImage("./images/img_TASK_07/TASK_07a_bg_1280x800.png");
        //cursorVisible = true;
        break;
    }
    case 8:
    {
        if (sounds[SOUNDS_TASK_7_NATURA].getIsPlaying())
            sounds[SOUNDS_TASK_7_NATURA].stop();
        background.loadImage("./images/img_TASK_08/TASK_08_bg_1280x800.png");
        //cursorVisible = true;
        break;
    }
    case 9:
    {
        if (sounds[SOUNDS_CUTSCENE_4].getIsPlaying())
            sounds[SOUNDS_CUTSCENE_4].stop();
        if (!sounds[SOUNDS_TASK_9_ATTIC].getIsPlaying())
            sounds[SOUNDS_TASK_9_ATTIC].play();
        background.loadImage("./images/img_TASK_09/task_09a_bg_1280x800.png");
        break;
    }
    case 10:
    {
        if (sounds[SOUNDS_TASK_9_ATTIC].getIsPlaying())
            sounds[SOUNDS_TASK_9_ATTIC].stop();
        sounds[SOUNDS_CUTSCENE_5].play();
        background.loadImage("./images/img_FRAMESTORY/framestory05b_bg_feedback_welldone_1280x800.png");
        taskState = TASK_STATE_CUT_SCENE;
        taskCompleted[10] = true;
        break;
    }
    }
    task = t;

    if (task > 0)
    {
        stringstream ss;
        if (task==2 && (slotsGuessed[SLOT_TASK_2_GLOVES] || slotsGuessed[SLOT_TASK_2_GOGGLES]))
            ss << "./texts/t" << (task) << "_oa.txt";
        else
            ss << "./texts/t" << (task) << "_k.txt";
        loadTextFromFile(ss.str());
    }
    fboUpdateRequested = true;
}

void testApp::updateFbo()
{
    ofLogNotice() << "updating static FBO content!";
    staticContent.begin();
    ofBackground(255, 0, 0);
    updateStaticContent();
    staticContent.end();
    fboUpdateRequested = false;
}

void testApp::loadTextFromFile(string s)
{
    ifstream fin;
    fin.open( ofToDataPath(s).c_str() );
    if (fin.is_open())
    {
        textString.erase();
        while(fin!=NULL) //as long as theres still text to be read
        {
            while (!fin.eof()) textString.push_back(fin.get());
        }
        if (textString.length() > 0)
            textString.erase(textString.length()-1,1);
        fin.close();
    } else {
        ofLogWarning() << "Could not open " << ofToDataPath(s).c_str() ;
    }

}

bool testApp::anyItemOnTable()
{
    for (int i = 0; i < 12; i++)
    {
        if (itemsOnTable[i]) return true;
    }
    return false;
}

int testApp::howManyItemsOnTable()
{
    int result = 0;
    for (int i = 0; i < 12; i++)
    {
        if (itemsOnTable[i]) result++;
    }
    return result;
}

void testApp::lockAnswer(bool correct)
{
    answerWasCorrect = correct;
    advanceTimer = ofGetElapsedTimeMillis();
    taskState = TASK_STATE_JUDGEMENT_DELAY;
    sounds[SOUNDS_SNAP].play();

    if (task==9) background.loadImage("./images/img_TASK_09/task_09c_bg_1280x800.png");
}

void testApp::announceWIN()
{
     if (!draggin && ( (task == 6 && itemsOnTable[ITEM_HNO3])
        || (task == 7 && itemsOnTable[ITEM_PAINT])
        || (task == 8 && itemsOnTable[ITEM_STAR]))) {
        sounds[SOUNDS_CORRECT].play();
        draggin = true;
        cout << "DRAGGING" << endl;
        taskState = TASK_STATE_TASK_GIVEN;
        rescan = true;
        cursorVisible = true;
        return;
    }

    lastActionOnTable = ofGetElapsedTimeMillis();
    if (task==0) background.loadImage("./images/img_FRAMESTORY/framestory01_bg_winter_1280x800.png");
    if (task==4) background.loadImage("./images/img_TASK_04/TASK_04b_bg_1280x800.png");
    if (task==5)
    {
        background.loadImage("./images/img_TASK_05/task_05b_bg_1280x800.png");
        if (sounds[SOUNDS_TASK_5_BOTTLE].getIsPlaying())
            sounds[SOUNDS_TASK_5_BOTTLE].stop();
    }
    if (task==7) background.loadImage("./images/img_TASK_07/TASK_07b_bg_1280x800.png");

    if (task==9) background.loadImage("./images/img_TASK_09/task_09d_bg_1280x800.png");

    taskState = TASK_STATE_CORRECT_ANSWER_GIVEN;
    advanceTimer = ofGetElapsedTimeMillis();
    taskCompleted[task] = true;
    advanceRequested = false;

    stringstream ss;
    ss << "./texts/t" << (task) << "_o.txt";
    loadTextFromFile(ss.str());
    lastItemOnTable = -1;

    if (task == 0)
    {
        taskState = TASK_STATE_CUT_SCENE;
        if (!sounds[SOUNDS_CUTSCENE_1].getIsPlaying())
            sounds[SOUNDS_CUTSCENE_1].play();
        signTableVisible = true;
    }
    else if (task == 2 && (!slotsGuessed[SLOT_TASK_2_GLOVES] || !slotsGuessed[SLOT_TASK_2_GOGGLES]))
    {
        sounds[SOUNDS_CORRECT].play();
        taskCompleted[task] = false;
        stringstream ss;
        ss << "./texts/t" << (task) << "_oa.txt";
        loadTextFromFile(ss.str());
    }
    else
    {
        sounds[SOUNDS_CORRECT].play();
        if (task==2 && sounds[SOUNDS_TASK_2_ALARM].getIsPlaying())
            sounds[SOUNDS_TASK_2_ALARM].stop();

        points += 3;
        if (task==8)
            points += 2;
        if (points > MAX_POINTS) points = MAX_POINTS;
    }
    fboUpdateRequested = true;
    draggin = false;
}

void testApp::announceFAIL()
{
    lastActionOnTable = ofGetElapsedTimeMillis();
    sounds[SOUNDS_FAIL].play();
    advanceTimer = ofGetElapsedTimeMillis();

    taskState = TASK_STATE_WRONG_ANSWER_GIVEN;
    if (points > 0) points--;

    stringstream ss;

    if (task==1)
    {
        loadTextFromFile("./texts/t1_v.txt");
    }
    else if (task == 2)
    {
        if (!itemsOnTable[ITEM_EARMUFFS])
        {
            loadTextFromFile("./texts/t2_k.txt");
            fboUpdateRequested = true;
            bool b = false;
            for (int i = 0; i < 12; i++)
            {
                if (i != ITEM_EARMUFFS && i != ITEM_GLASSES && i != ITEM_GLOVES)
                {
                    if (itemsOnTable[i])
                        b = true;
                }
            }
            if (b)
                loadTextFromFile("./texts/te.txt");
            return;
        }
        else
        {
            loadTextFromFile("./texts/t2_v.txt");
            return;
        }
    }
    if (task == 3)
    {
        background.loadImage("./images/img_TASK_03/task_03b_bg_1280x800.png");
        sounds[SOUNDS_TASK_3_FIRE].play();
        loadTextFromFile("./texts/t3_v.txt");
        return;
    }
    else if (task == 4)
    {
        if (!itemsOnTable[ITEM_MATCHES])
        {
            loadTextFromFile("./texts/te.txt");
            return;
        }
        sounds[SOUNDS_TASK_4_EXPLOSION].play();
        background.loadImage("./images/img_TASK_04/TASK_04c_bg_1280x800.png");
        loadTextFromFile("./texts/t4_v.txt");
        return;
    }
    else if (task == 5)
    {
        loadTextFromFile("./texts/t5_v.txt");
        return;
    }
    else if (task==6)
    {
        if (!itemsOnTable[ITEM_HNO3]) {
            loadTextFromFile("./texts/te.txt");
            draggin = false;
        }
        else if (lastSlotGuessed==SLOT_TASK_6_1)
            loadTextFromFile("./texts/t6_va.txt");
        else if (lastSlotGuessed==SLOT_TASK_6_2)
            loadTextFromFile("./texts/t6_vb.txt");
        return;
    }
    else if (task==7)
    {
        if (!itemsOnTable[ITEM_PAINT]) {
            draggin = false;
            loadTextFromFile("./texts/te.txt");
        }
        else return;
    }
    else if (task==8)
    {
        if (!itemsOnTable[ITEM_STAR]) {
            loadTextFromFile("./texts/te.txt");
            draggin = false;
        }
        else
            loadTextFromFile("./texts/t8_v.txt");
        return;
    }
    else if (task==9)
    {
        loadTextFromFile("./texts/te.txt");
    }
    //loadTextFromFile(ss.str());
    fboUpdateRequested = true;
}

void testApp::newGame()
{
    silence();
    for (int i = 0; i < 11; i++)
    {
        taskCompleted[i] = false;
    }
    for (int i = 0; i < 10; i++)
    {
        slotsGuessed[i] = false;
    }
    for (int i = 0; i < 12; i++)
    {
        itemsOnTable[i] = false;
    }
    initNewTask(0);
    points = 0;
    signTableVisible = false;
    paintGoingTo = 666;
    cursorX = 0;
    cursorY = 0;
    draggin = false;
    lastActionOnTable = ofGetElapsedTimeMillis();
}


void testApp::keyPressed(int key)
{
    switch (key)
    {
    case 'a':
        nearT--;
        cout << "nearT " << nearT << endl;
        break;
    case 's':
        nearT++;
        cout << "nearT " << nearT << endl;
        break;
    case 'd':
        depthFilterOn = !depthFilterOn;
        break;
    case 'f':
        fboMode = !fboMode;
        ofLogNotice()  << "FBO mode is " << fboMode;
        staticContent.begin();
        ofBackground(255, 0, 0);
        updateStaticContent();
        staticContent.end();
        break;
    case 'r':
        newGame();
        break;
    case 'z':
        drawGrayDiff = !drawGrayDiff;

        break;
    case 'c':
        drawDepthImage = !drawDepthImage;
        break;
    case 'x':
        int t = task;
        newGame();
        initNewTask(t);
        break;

    }

    if( key == ' ' )
    {
        advanceRequested = true;
    }
    else if( key == 'p' )
    {
        threshold = MAX( 0, threshold-1 );
        cout << threshold << endl;
    }
    else if( key == 'i' || key == '=' )
    {
        threshold = MIN( 255, threshold+1 );
    }
    else if( key == 'b' )
    {
        backgroundSubOn = !backgroundSubOn;
        //bLearnBakground = true;
    }
}
//--------------------------------------------------------------
void testApp::keyReleased(int key)
{

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y )
{
    cout << "x:" << (mouseX - postMouseX) << " y:" << (mouseY - postMouseY) <<  "mx:" << mouseX << " my:" << mouseY << endl;
    //mouseMovedLast = ofGetElapsedTimeMillis();
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button)
{

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button)
{
    debugGraphics = !debugGraphics;
    //midiOut->sendNoteOn(3,3);
    //postMouseX = mouseX;
    //postMouseY = mouseY;
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button)
{

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h)
{

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg)
{

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo)
{

}

//--------------------------------------------------------------
/*void testApp::newMidiMessage(ofxMidiMessage& msg)
{

    // make a copy of the latest message
    cout << "ha" << endl;
}*/

bool testApp::projection(float x1, float y1, float x2, float y2, float a1, float b1, float * a2, float * b2, bool below, bool s, bool e)
{
    float k = (y2 - y1) / (x2 - x1);
    float c = -((y2 - y1) / (x2 - x1))*x1 + y1;
    if ((!below && b1 < k*a1 + c) || (below && b1 > k * a1 + c))
    {
        float kn = -1/k;
        float d = -kn * x1 + y1;

        if ((below && b1 < kn * a1 + d) || (!below && b1 > kn * a1 + d))
        {
            if (s)
            {
                *a2 = x1;
                *b2 = y1;
                return true;
            }
            else
            {
                *a2 = a1;
                *b2 = b1;
                return false;
            }
        }

        d = -kn * x2 + y2;

        if ((!below && b1 < kn * a1 + d) || (below && b1 > kn * a1 + d))
        {
            if (e)
            {
                *a2 = x2;
                *b2 = y2;
                return true;
            }
            else
            {
                *a2 = a1;
                *b2 = b1;
                return false;
            }
        }

        d = -kn * a1 + b1;

        *a2 = (c - d) / (-(k - kn));
        *b2 = *a2 * k + c;
        return true;

    }
    else
    {
        *a2 = a1;
        *b2 = b1;
    }
    return false;
}
